# SCC4

Hands-on da SCC4.

Modifiquei as urls, estavam dando problema com o CURL do windows, retirei os {}
da primeira funcao, e os % da funcao nome bibliografico.

Execucao:

Rodar o arquivo .jar localizado na pasta target.
Realizar as requisicoes pelo curl.

Nas funcoes da calculadora, passar a funcao a ser executada e as fracoes em
formato de lista.

Exemplo:

curl http://localhost:8080/divisao?valores=1,2,5,4
Representa a divisao da fracao 1/2 pela fracao 5/4.