package com.example.handson;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

@RestController
public class Controller {
    
    @RequestMapping(path = "/listaReversa", method = RequestMethod.GET)
    public ResponseEntity<String> ListaReversa(@RequestParam String lista) {
        
        String[] saida = lista.split(",");
        ArrayUtils.reverse(saida);
        
        return new ResponseEntity<>(Arrays.toString(saida), HttpStatus.OK);
    }

    @RequestMapping(path = "/imprimirImpares", method = RequestMethod.GET)
    public ResponseEntity<String> imprimirImpares(@RequestParam String lista) {
        
        String[] valores_entrada = lista.split(",");
        List<Integer> valores_saida = new ArrayList<>();
        
        for (int i = 0; i < valores_entrada.length; i++) {
            int temp = Integer.parseInt(valores_entrada[i]);
            if((temp % 2) != 0){
                valores_saida.add(temp);
            }    
        }

        return new ResponseEntity<>(valores_saida.toString(), HttpStatus.OK);
    }

    @RequestMapping(path = "/tamanho", method = RequestMethod.GET)
    public ResponseEntity<Integer> tamanho(@RequestParam String palavra) {
        
        return new ResponseEntity<>(palavra.length(), HttpStatus.OK);
    }

    @RequestMapping(path = "/maiusculas", method = RequestMethod.GET)
    public ResponseEntity<String> maiusculas(@RequestParam String palavra) {

        String saida = palavra.toUpperCase();
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }

    @RequestMapping(path = "/vogais", method = RequestMethod.GET)
    public ResponseEntity<String> vogais(@RequestParam String palavra) {

        String saida = palavra.toLowerCase();
        saida = saida.replaceAll("[bcdfghjklmnpqrstvwxyz]", "");
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }

    @RequestMapping(path = "/consoantes", method = RequestMethod.GET)
    public ResponseEntity<String> consoantes(@RequestParam String palavra) {

        String saida = palavra.toLowerCase();
        saida = saida.replaceAll("[aeiou]", "");
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }

    @RequestMapping(path = "/nomeBibliografico", method = RequestMethod.GET)
    public ResponseEntity<String> nomeBibliografico(@RequestParam String nome) {

        String[] temp = nome.split(",");
        String saida = temp[temp.length - 1].toUpperCase();
        String[] primeiro_nome_temp = Arrays.copyOfRange(temp, 0, temp.length - 1);
        String primeiro_nome = String.join(" ", primeiro_nome_temp);
        saida = saida + "," + primeiro_nome;
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }

    @RequestMapping(path = "/saque", method = RequestMethod.GET)
    public ResponseEntity<String> saque(@RequestParam Integer valor) {

        int notas[] = {5, 3};
        int[] quantia_nota = new int[2];
        int valor_total = valor;
        
        for (int i = 0; i < 2; i++){
            if(valor_total >= notas[i]){
                quantia_nota[i] = valor/notas[i];
                valor_total = valor_total - quantia_nota[i] * notas[i];
            }
        }

        String saida = "Saque R$" + valor + ": " + quantia_nota[0] + " notas de R$5 e " + quantia_nota[1] + " nota de R$3.";
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }

    public Integer calculaMMC(Integer a, Integer b){

        if (b == 0){
            return a;
        }
        return calculaMMC(b, a % b);
    }

    @RequestMapping(path = "/adicao", method = RequestMethod.GET)
    public ResponseEntity<String> adicao(@RequestParam String valores) {

        String[] temp = valores.split(",");
        Integer numerador1 = Integer.parseInt(temp[0]);
        Integer denominador1 = Integer.parseInt(temp[1]);
        Integer numerador2 = Integer.parseInt(temp[2]);
        Integer denominador2 = Integer.parseInt(temp[3]);

        Integer numerador_saida = numerador1 + numerador2;
        Integer denominador_saida = calculaMMC(denominador1, denominador2);

        String saida = numerador_saida + "/" + denominador_saida;
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }

    @RequestMapping(path = "/subtracao", method = RequestMethod.GET)
    public ResponseEntity<String> subtracao(@RequestParam String valores) {

        String[] temp = valores.split(",");
        Integer numerador1 = Integer.parseInt(temp[0]);
        Integer denominador1 = Integer.parseInt(temp[1]);
        Integer numerador2 = Integer.parseInt(temp[2]);
        Integer denominador2 = Integer.parseInt(temp[3]);

        Integer numerador_saida = numerador1 - numerador2;
        Integer denominador_saida = calculaMMC(denominador1, denominador2);

        String saida = numerador_saida + "/" + denominador_saida;
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }

    @RequestMapping(path = "/multiplicacao", method = RequestMethod.GET)
    public ResponseEntity<String> multiplicacao(@RequestParam String valores) {

        String[] temp = valores.split(",");
        Integer numerador1 = Integer.parseInt(temp[0]);
        Integer denominador1 = Integer.parseInt(temp[1]);
        Integer numerador2 = Integer.parseInt(temp[2]);
        Integer denominador2 = Integer.parseInt(temp[3]);

        Integer numerador_saida = numerador1 * numerador2;
        Integer denominador_saida =denominador1 * denominador2;

        String saida = numerador_saida + "/" + denominador_saida;
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }

    @RequestMapping(path = "/divisao", method = RequestMethod.GET)
    public ResponseEntity<String> divisao(@RequestParam String valores) {

        String[] temp = valores.split(",");
        Integer numerador1 = Integer.parseInt(temp[0]);
        Integer denominador1 = Integer.parseInt(temp[1]);
        Integer numerador2 = Integer.parseInt(temp[2]);
        Integer denominador2 = Integer.parseInt(temp[3]);

        Integer numerador_saida = numerador1 * denominador2;
        Integer denominador_saida =denominador1 * numerador2;

        String saida = numerador_saida + "/" + denominador_saida;
        return new ResponseEntity<>(saida, HttpStatus.OK);
    }
}